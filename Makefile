TGT = i686-elf
CC = $(TGT)-gcc
AS = $(TGT)-as
INCS = -I.
FLAGS := -O2 -ffreestanding
CFLAGS += -std=gnu99
CFLAGS += -Wall -Wextra -pedantic
CFLAGS += -Werror -pedantic-errors
CFLAGS += $(FLAGS)
LFLAGS := -T linker.ld -nostdlib
LFLAGS += $(FLAGS)


os.bin: boot.o kernel.o
	$(CC) $(LFLAGS) -o os.bin terminal.o utils.o boot.o kernel.o

boot.o:
	$(AS) boot.s -o boot.o

kernel.o: terminal.o kernel.c
	$(CC) $(CFLAGS) -c kernel.c -o kernel.o

terminal.o: utils.o terminal.c
	$(CC) $(CFLAGS) -c terminal.c -o terminal.o

utils.o: utils.c
	$(CC) $(CFLAGS) -c utils.c -o utils.o

clean:
	rm -rf iso/
	rm -rf *.iso
	rm -rf *.o
	rm -rf *.bin


iso: os.bin
	mkdir -p iso/boot/grub
	cp os.bin iso/boot
	cp grub.cfg iso/boot/grub
	grub-mkrescue -o os.iso iso/
