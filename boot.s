# boot.s
#
# bootstrap assembly stub to setup the processor
# and call the kernel main function
################################################
	
# declare multiboot header

.set ALIGN,    1<<0
.set MEMINFO,  1<<1
.set FLAGS,    ALIGN | MEMINFO
.set MAGIC,    0x1BADB002
.set CHECKSUM, -(MAGIC + FLAGS)

.section .multiboot
.align 4
.long MAGIC
.long FLAGS
.long CHECKSUM
	
# set up a temporary stack

.section .bootstrap_stack
stack_bottom:
.skip 16384 # 16 KiB
stack_top:

# we are now ready to start working

.section .text
.global _start
.type _start, @function
_start:

	# we are now in kernel mode.
	# next, set up the actual stack
	movl $stack_top, %esp

	# then jump in the actual kernel
	call kmain

	# if we ever return from the kernel, we put the cpu in an idle loop
	cli
	hlt
.Lloop:
	jmp .Lloop

# useful for debugging, call tracing etc
.size _start, . - _start
