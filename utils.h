/* utils.h
 *
 * helper functions
 *******************/

#ifndef __UTILS_H__
#define __UTILS_H__

#include <stddef.h>

size_t strlen(const char* str);

#endif
