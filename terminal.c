/* terminal.c
 *
 * module for graphical output on the terminal
 *********************************************/

#include <stddef.h>
#include <stdint.h>
#include "terminal.h"
#include "utils.h"

typedef struct {
    size_t   MAX_HEIGHT;
    size_t   MAX_WIDTH;
    size_t   row;
    size_t   column;
    VGAColor color;
    uint16_t *buffer;
} Terminal;

Terminal term;

static uint8_t mkcolor(uint8_t fg, uint8_t bg);
static uint16_t mkVGAentry(char c, uint8_t color);
static void putCharAt(char c, uint8_t color, size_t i, size_t j);
static void mvcursor();
static void newline();
static void scroll1();

/* clear the terminal */
void term_clear(void) 
{
    term.row    = 0;
    term.column = 0;

    for(size_t i = 0; i < term.MAX_HEIGHT; i++) {
	for(size_t j = 0; j < term.MAX_WIDTH; j++) {
	    const size_t k = i * term.MAX_WIDTH + j;
	    term.buffer[k] = mkVGAentry(' ', term.color);
	}
    }
}
    

/* initialize terminal to default values and draw all entries */
void term_init(size_t max_width, size_t max_height, uint8_t fg, uint8_t bg) 
{
    term.MAX_HEIGHT = max_height;
    term.MAX_WIDTH  = max_width;
    term.row    = 0;
    term.column = 0;
    term.color  = mkcolor(fg,bg);
    term.buffer = (uint16_t*) 0xB8000;

    for(size_t i = 0; i < max_height; i++) {
	for(size_t j = 0; j < max_width; j++) {
	    const size_t k = i * max_width + j;
	    term.buffer[k] = mkVGAentry(' ', term.color);
	}
    }
}

/* draw char using current term settings, and update cursor position */ 
void term_putChar(char c)
{
    if(c == '\n') {
	newline();
    } else {
	putCharAt(c,term.color,term.column, term.row);
	mvcursor();
    }
}


void term_putStr(const char *str)
{
    size_t len = strlen(str);
    for(size_t i = 0; i < len; i++)
	term_putChar(str[i]);
}

void term_setcolor(uint8_t fg, uint8_t bg)
{
    term.color = mkcolor(fg,bg);
}

/*** static functions ***/

/* return color suitable for drawing on terminal */
uint8_t mkcolor(uint8_t fg, uint8_t bg)
{
    return fg | bg << 4;
}

/* create an entry for drawing */
uint16_t mkVGAentry(char c, uint8_t color)
{
    uint16_t c16 = c;
    uint16_t color16 = color;
    return c16 | color16 << 8;
}

/* draw entry at specified position */
void putCharAt(char c, uint8_t color, size_t i, size_t j)
{
    const size_t k = j * term.MAX_WIDTH + i;
    term.buffer[k] = mkVGAentry(c,color);
} 

void mvcursor()
{
    if(++term.column == term.MAX_WIDTH)
	newline();
}

void newline() {
    term.column = 0;
    if(term.row + 1 == term.MAX_HEIGHT)
	scroll1();
    else
	++term.row;
}

void scroll1()
{
    for(size_t i = 0; i < term.MAX_HEIGHT - 1; i++) {
	for(size_t j = 0; j < term.MAX_WIDTH; j++) {
	    const size_t k1 = i     * term.MAX_WIDTH + j;
	    const size_t k2 = (i+1) * term.MAX_WIDTH + j;
	    term.buffer[k1] = term.buffer[k2];
	}
    }
    
    for(size_t j = 0; j < term.MAX_WIDTH; j++) {
	const size_t k = (term.MAX_HEIGHT - 1) * term.MAX_WIDTH + j;
	term.buffer[k] = mkVGAentry(' ', term.color);
    }
}
