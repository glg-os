#include <stdbool.h> 
#include <stddef.h>
#include <stdint.h>
#include "terminal.h"

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 24;
 
void kmain()
{
    term_init(VGA_WIDTH, VGA_HEIGHT, COLOR_BLACK, COLOR_LIGHT_GREY);
   
    term_putStr("test1\n");
    term_putStr("test2\n");

    for(uint8_t i = 0; i < VGA_HEIGHT - 3; i++) 
	term_putStr("test\n");
   
    term_putStr("test3\n");
    
    term_clear();
    
}

