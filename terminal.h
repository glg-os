/* terminal.h
 *
 * module for graphical output on the terminal 
 *********************************************/

#ifndef __TERMINAL_H__
#define __TERMINAL_H__

/* Hardware text mode color constants. */
typedef enum {
	COLOR_BLACK = 0,
	COLOR_BLUE = 1,
	COLOR_GREEN = 2,
	COLOR_CYAN = 3,
	COLOR_RED = 4,
	COLOR_MAGENTA = 5,
	COLOR_BROWN = 6,
	COLOR_LIGHT_GREY = 7,
	COLOR_DARK_GREY = 8,
	COLOR_LIGHT_BLUE = 9,
	COLOR_LIGHT_GREEN = 10,
	COLOR_LIGHT_CYAN = 11,
	COLOR_LIGHT_RED = 12,
	COLOR_LIGHT_MAGENTA = 13,
	COLOR_LIGHT_BROWN = 14,
	COLOR_WHITE = 15,
} VGAColor;

/* clear the terminal */
void term_clear(void);

/* initialize terminal to default values and draw all entries */
void term_init(size_t max_width, size_t max_height, uint8_t fg, uint8_t bg);

/* draw char using current term settings, and update cursor position */ 
void term_putChar(char c);

/* draw string using current term settings, and update cursor position */
void term_putStr(const char *str);

/* set terminal color */
void term_setcolor(uint8_t fg, uint8_t bg); 

#endif

